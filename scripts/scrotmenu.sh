#!/bin/bash

rofi_command="rofi -theme themes/scrotmenu.rasi"

while [ $# -gt 0 ]; do
  case "$1" in
    --screen)
      screen_exec="$2"
      ;;
    --area)
      area_exec="$2"
      ;;
    --window)
      window_exec="$2"
      ;;
    *)
      echo "Error: Invalid argument ${1}"
      exit 1
  esac
  shift
  shift
done

### Options ###
screen=""
area=""
window=""
# Variable passed to rofi
options="$screen\n$area\n$window"

chosen="$(echo -e "$options" | $rofi_command -dmenu -selected-row 1)"
case $chosen in
    $screen)
        [[ ! -z $screen_exec ]] && exec $screen_exec || scrot
        ;;
    $area)
        [[ ! -z $area_exec ]] && exec $area_exec || scrot -s
        ;;
    $window)
        [[ ! -z $window_exec ]] && exec $window_exec || scrot -u
        ;;
esac

