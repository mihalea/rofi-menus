#!/bin/bash

rofi_command="rofi -theme themes/powermenu.rasi"

while [ $# -gt 0 ]; do
  case "$1" in
    --poweroff)
      poweroff_exec="$2"
      ;;
    --reboot)
      reboot_exec="$2"
      ;;
    --lock)
      lock_exec="$2"
      ;;
    --suspend)
      suspend_exec="$2"
      ;;
    --logout)
      logout_exec="$2"
      ;;
    *)
      echo "Error: Invalid argument ${1}"
      exit 1
  esac
  shift
  shift
done

### Options ###
power_off=""
reboot=""
lock=""
suspend="鈴"
log_out=""
# Variable passed to rofi
options="$lock\n$reboot\n$power_off\n$suspend\n$log_out"

chosen="$(echo -e "$options" | $rofi_command -dmenu -selected-row 2)"
case $chosen in
    $power_off)
        [[ ! -z $poweroff_exec ]] && exec $poweroff_exec || systemctl poweroff
        ;;
    $reboot)
        [[ ! -z $reboot_exec ]] && exec $reboot_exec || systemctl reboot
        ;;
    $lock)
        [[ ! -z $lock_exec ]] && exec $lock_exec || light-locker-command -l
        ;;
    $suspend)
        [[ ! -z $suspend_exec ]] && exec $suspend_exec || systemctl suspend
        ;;
    $log_out)
        [[ ! -z $logout_exec ]] && exec $logout_exec || i3-msg exit
        ;;
esac

